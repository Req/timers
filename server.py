#!/usr/bin/env python3

import os
import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
	s.bind(("0.0.0.0", 12333))
	s.listen()
	while True:
		conn, _ = s.accept()
		data = conn.recv(1024)
		if len(data) > 0:
			os.system("aplay alert.wav")
		conn.close()
