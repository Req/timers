
#include <array>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstring>

#include <pico/multicore.h>

#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <Fonts/FreeMono24pt7b.h>
#include <light_CD74HC4067.h>
#include <WiFi.h>

#if 1
#define log(...) Serial.printf(__VA_ARGS__)
#else
#define log(...)
#endif

static constexpr const char *SSID = "TELUS3428";
static constexpr const char *PSK = "rvrhn5pfp6";
static const IPAddress SERVER{192, 168, 1, 200};
static const std::uint16_t PORT = 12333;

struct alignas(4) StatusBits {
	std::uint8_t index : 2;
	bool selected : 1;
	bool editing : 1;
	std::uint32_t status : 28;

	[[nodiscard]] static StatusBits wrap(const std::uint32_t value) noexcept {
		// no bit_cast available
		StatusBits sb;
		std::memcpy(&sb, &value, sizeof(sb));
		return sb;
	}

	[[nodiscard]] std::uint32_t unwrap() const noexcept {
		std::uint32_t value;
		std::memcpy(&value, this, sizeof(value));
		return value;
	}
};

[[nodiscard]] static bool operator==(const StatusBits lhs, const StatusBits rhs) noexcept {
	return !std::memcmp(&lhs, &rhs, sizeof(lhs));
}

[[nodiscard]] static bool operator!=(const StatusBits lhs, const StatusBits rhs) noexcept {
	return !(lhs == rhs);
}

class Clock {
private:
	std::uint64_t last_poll = 0;

public:
	[[nodiscard]] std::uint64_t poll_delta() noexcept {
		/*
		 * `millis` will eventually overflow. Since this is unsigned
		 * arithmetic, delta should still be calculated correctly.
		 */
		const auto now = millis();
		const auto delta = now - last_poll;
		last_poll = now;
		return delta;
	}
};

class Timer {
private:
	std::uint64_t duration;
	std::uint64_t elapsed = 0;

public:
	explicit Timer() noexcept : Timer{0} {}
	explicit Timer(const std::uint64_t duration) noexcept : duration{duration} {}

	void apply_delta(const std::uint64_t delta) noexcept {
		if (is_expired()) {
			return;
		}

		if (elapsed + delta < elapsed) {
			// overflowed
			elapsed = duration;
		} else {
			elapsed += delta;
		}
	}

	void destroy() noexcept {
		duration = 0;
		elapsed = 0;
	}

	[[nodiscard]] bool is_expired() const noexcept {
		return elapsed >= duration;
	}

	[[nodiscard]] bool is_empty() const noexcept {
		return duration == 0;
	}

	[[nodiscard]] std::uint16_t remaining_seconds() const noexcept {
		if (is_expired() || is_empty()) {
			return 0;
		}

		auto remaining = duration - elapsed;
		remaining += 1000 - (remaining % 1000);
		return remaining / 1000;
	}
};

class KeyBuffer {
private:
	std::array<std::int8_t, 4> buffer;
	std::uint8_t index = 0;

public:
	void push_digit(const std::uint8_t digit) noexcept {
		if (index >= buffer.size()) {
			return;
		}
		buffer[index] = digit;
		++index;
	}

	[[nodiscard]] bool is_empty() const noexcept {
		return index == 0;
	}

	void reset() noexcept {
		index = 0;
	}

	[[nodiscard]] std::uint64_t build() noexcept {
		int minutes;
		int seconds;
		// leftmost digit starts at [0]
		switch (index) {
		case 1: // 00:0s
			minutes = 0;
			seconds = buffer[0];
			break;
		case 2: // 00:ss
			minutes = 0;
			seconds = buffer[0] * 10 + buffer[1];
			break;
		case 3: // m:ss
			minutes = buffer[0];
			seconds = buffer[1] * 10 + buffer[2];
			break;
		case 4: // mm:ss
			minutes = buffer[0] * 10 + buffer[1];
			seconds = buffer[2] * 10 + buffer[3];
			break;
		case 0:
			[[fallthrough]];
		default:
			return 0;
		}
		return ((minutes * 60) + seconds) * 1000;
	}
};

class CoreContainer {
private:
	CD74HC4067 mux{18, 19, 20, 21};
	std::array<bool, 16> keys_pressed{false};
	Clock clock;
	std::array<Timer, 3> timers;
	std::int8_t current_timer_index = 0;
	KeyBuffer key_buffer;
	std::uint8_t packet_id_counter = 0;

	void poll_input() noexcept {
		for (auto i = 0; i < 16; ++i) {
			mux.channel(i);
			if (analogRead(A0) > 200) {
				if (!keys_pressed[i]) {
					switch (i) {
					case 0: [[fallthrough]];
					case 1: [[fallthrough]];
					case 2: [[fallthrough]];
					case 3: [[fallthrough]];
					case 4: [[fallthrough]];
					case 5: [[fallthrough]];
					case 6: [[fallthrough]];
					case 7: [[fallthrough]];
					case 8: [[fallthrough]];
					case 9: // digit
						if (timers[current_timer_index].is_empty()) {
							key_buffer.push_digit(i);
							log("pushed digit %d, sum is now %llums\r\n", i, key_buffer.build());
						}
						break;
					case 10: // cancel
						timers[current_timer_index].destroy();
						key_buffer.reset();
						log("canceled (%d)\r\n", static_cast<int>(current_timer_index));
						break;
					case 11: { // accept
						auto &timer = timers[current_timer_index];
						if (timer.is_empty() && !key_buffer.is_empty()) {
							const auto duration = key_buffer.build();
							key_buffer.reset();
							if (duration > 0) {
								timer = Timer{duration};
								log("accepted: timer %d is set to %llums\r\n", static_cast<int>(current_timer_index), duration);
							}
						}
						break;
					}
					case 12: // cycle
						current_timer_index = (current_timer_index + 1) % timers.size();
						key_buffer.reset();
						log("cycled to %d\r\n", static_cast<int>(current_timer_index));
						break;
					case 13: [[fallthrough]];
					case 14: [[fallthrough]];
					case 15: // nothing connected
						break;
					}
				}
				keys_pressed[i] = true;
			} else {
				keys_pressed[i] = false;
			}
		}
	}

	void fire_alarm() noexcept {
		WiFiClient client;
		client.connect(SERVER, PORT);
		// the content is mostly irrelevant
		client.print(" ");
	}

	void poll_timers() noexcept {
		const auto delta = clock.poll_delta();
		for (auto i = 0; i < static_cast<int>(timers.size()); ++i) {
			auto &timer = timers[i];

			std::uint32_t remaining = 0;
			if (!timer.is_empty()) {
				timer.apply_delta(delta);
				if (timer.is_expired()) {
					log("timer %d has expired\r\n", i);
					fire_alarm();
					timer.destroy();
				} else {
					remaining = timer.remaining_seconds();
				}
			}

			const auto selected = i == current_timer_index;
			const auto editing = selected && !key_buffer.is_empty();
			if (editing) {
				remaining = key_buffer.build() / 1000;
			}

			StatusBits sb;
			sb.status = remaining;
			sb.index = i;
			sb.selected = selected;
			sb.editing = editing;
			rp2040.fifo.push_nb(sb.unwrap());
		}
	}

public:
	void setup() noexcept {
		pinMode(A0, INPUT);

		int status = WL_IDLE_STATUS;
		while (status != WL_CONNECTED) {
			status = WiFi.begin(SSID, PSK);
			delay(1000);
		}
	}

	void poll() noexcept {
		poll_input();
		poll_timers();
	}
};

static void format_time(char *buffer, std::uint32_t seconds) noexcept {
	const auto minutes = seconds / 60;
	seconds = seconds % 60;
	std::sprintf(buffer, "%02lu:%02lu", minutes, seconds);
}

class UiContainer {
private:
	Adafruit_ILI9341 tft{0, 4, 7, 6, 1, 8};
	std::array<StatusBits, 3> cache{};

	void redraw() {
		static std::array<std::array<char, 6>, 3> cached_strings;
		decltype(cached_strings) strings;

		constexpr auto LEFT_MARGIN = 50;
		constexpr auto TOP_MARGIN = 70;
		constexpr auto CHAR_SPACING = 40;
		constexpr auto LINE_SPACING = 70;

		for (auto i = 0; i < static_cast<int>(strings.size()); ++i) {
			format_time(strings[i].data(), cache[i].status);

			for (auto j = 0; j < static_cast<int>(strings[i].size()); ++j) {
				auto *expected = &cached_strings[i][j];
				const auto actual = strings[i][j];
				if (*expected != actual) {
					*expected = actual;

					tft.fillRect(
						LEFT_MARGIN + j * CHAR_SPACING + 6,
						TOP_MARGIN + i * LINE_SPACING - 60,
						CHAR_SPACING + 2,
						LINE_SPACING,
						ILI9341_BLACK
					);
					tft.drawChar(
						LEFT_MARGIN + j * CHAR_SPACING,
						TOP_MARGIN + i * LINE_SPACING,
						static_cast<unsigned char>(actual),
						ILI9341_WHITE,
						ILI9341_BLACK,
						2
					);
				}
			}

			static std::uint8_t last_selected = 255;
			if (cache[i].selected && i != last_selected) {
				constexpr auto SIZE = 16;
				constexpr auto START_X = LEFT_MARGIN + 6 * CHAR_SPACING - SIZE / 2;
				constexpr auto START_Y = TOP_MARGIN / 2;

				tft.fillRect(
					START_X,
					START_Y + last_selected * LINE_SPACING,
					SIZE,
					SIZE,
					ILI9341_BLACK
				);
				tft.fillRect(
					START_X,
					START_Y + i * LINE_SPACING,
					SIZE,
					SIZE,
					ILI9341_GREEN
				);

				last_selected = i;
			}
		}
	}

public:
	void setup() noexcept {
		pinMode(A1, INPUT);

		std::memset(cache.data(), 0, sizeof(cache));

		tft.begin();
		tft.setRotation(3);
		tft.setFont(&FreeMono24pt7b);
		tft.fillScreen(ILI9341_BLACK);
		redraw();
	}

	void poll() noexcept {
		for ([[maybe_unused]] auto &_ : cache) {
			StatusBits sb;
			if (!rp2040.fifo.pop_nb(reinterpret_cast<std::uint32_t*>(&sb))) {
				break;
			}

			if (cache[sb.index] != sb) {
				log(
					"ui: timer %d is now: status=%d, selected=%d, editing=%d\r\n",
					static_cast<int>(sb.index),
					static_cast<int>(sb.status),
					static_cast<int>(sb.selected),
					static_cast<int>(sb.editing)
				);
			}
			cache[sb.index] = sb;
		}
		redraw();
	}
};

static CoreContainer *core_container = nullptr;
static UiContainer *ui_container = nullptr;

void setup() noexcept {
	Serial.begin(9600);

	static CoreContainer container;
	container.setup();
	core_container = &container;
}

void loop() noexcept {
	core_container->poll();
}

void setup1() noexcept {
	static UiContainer container;
	container.setup();
	ui_container = &container;
}

void loop1() noexcept {
	if (ui_container == nullptr) {
		return;
	}
	ui_container->poll();
}

/*
 * Photoresistor readings (pin A1):
 * - <10: low light
 * - <20: normal light
 * - <40: high light
 * - higher: very light light
 */
