
.DEFAULT_GOAL := build
BIN := build/rp2040.rp2040.rpipicow/timers.ino.uf2

.PHONY: build
build: $(BIN)

$(BIN): timers.ino
	arduino-cli compile -e --warnings all --fqbn rp2040:rp2040:rpipicow $<

.PHONY: deploy
deploy: $(BIN)
	sudo umount -q /mnt || true
	sudo mount /dev/sda1 /mnt && sudo cp $(BIN) /mnt
	sudo umount -q /mnt || true

.PHONY: clean
clean:
	rm -rf build/

.PHONY: install
install:
	mkdir -p ~/.local/share/systemd/user
	cp timers-server.service ~/.local/share/systemd/user/
	systemctl --user enable --now timers-server
